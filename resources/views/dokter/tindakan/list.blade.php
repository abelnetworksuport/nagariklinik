@extends('layout.admin')

@yield('title', 'List Tindakan')

@section('content')
<section role="main" class="content-body">
        <header class="page-header">
        <h2>Dokter</h2>
    
        <div class="right-wrapper text-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="index.html">
                        <i class="fas fa-home"></i>
                    </a>
                </li>
                <li><span>Dokter</span></li>
                <li><span>List Tindakan</span></li>
            </ol>
    
            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fas fa-chevron-left"></i></a>
        </div>
        </header>
        <!-- header atas -->
        
        <div class="row">
			<div class="col-lg-12">
				<section class="card">
				    <header class="card-header">
						<div class="card-actions">
                            
							<a href="#" class="card-action card-action-toggle" data-card-toggle></a>
							<a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
						</div>
						
						<h2 class="card-title">List Tindakan</h2>
                    </header>
                    
                    <div class="card">
                        <div class="card-body">
                            
                            
                            <div class="table-responsive m-t-40">
                                <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Rekam Medis</th>
                                            <th>Tanggal</th>
                                            <th>Nama Pasien</th>
                                            <th>Nama Dokter</th>
                                            <th>Poli</th>
                                            <th>Status</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>No</th>
                                            <th>Tanggal</th>
                                            <th>Rekam Medis</th>
                                            <th>Nama Pasien</th>
                                            <th>Nama Dokter</th>
                                            <th>Poli</th>
                                            <th>Status</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                        <tr style="text-align:center">
                                            <td >1</td>
                                            <td>RM-001</td>
                                            <td>01 Februari 2019</td>
                                            <td>Helmay</td>
                                            <td>Dr.Agus Setyadi</td>
                                            <td>Poli Umum</td>
                                            
                                            <td>
                                                <span class="badge badge-danger">Pending</span>
                                            </td>
                                            <td>
                                                <a class="btn-sm btn-success" title="Lihat Tindakan !" style="margin-right:5px" href="{{ route('lihat_tindakan') }}"> <i class="fa fa-eye" aria-hidden="true"></i></a>
                                                <a class="btn-sm btn-warning" title="Buat Tindakan!" style="margin-right:5px" href="{{ route('tindakan') }}"> <i class="fa fa-plus" aria-hidden="true"></i> Proses </a>
                                            </td>  
                                        </tr>
                                        <tr style="text-align:center">
                                            <td >2</td>
                                            <td>RM-001</td>
                                            <td>20 Januari 2019</td>
                                            <td>Anto</td>
                                            <td>Drg.Anita</td>
                                            <td>Poli Gigi</td>
                                            
                                            <td>
                                                <span class="badge badge-danger">Pending</span>
                                            </td>
                                            <td>
                                                <a class="btn-sm btn-success" title="Lihat Tindakan !" style="margin-right:5px" href="{{ route('lihat_tindakan') }}"> <i class="fa fa-eye" aria-hidden="true"></i></a>
                                                <a class="btn-sm btn-warning" title="Buat Tindakan!" style="margin-right:5px" href="{{ route('tindakan') }}"> <i class="fa fa-plus" aria-hidden="true"></i> Proses </a>
                                            </td>  
                                        </tr>
                                        <tr>
                                            <td style="text-align:center">3</td>
                                            <td style="text-align:center">19 Januari 2019</td>
                                            <td>RM-001</td>
                                            <td style="text-align:center">Milea</td>
                                            <td style="text-align:center">Dr.Dilan</td>
                                            <td style="text-align:center">Poli Klinik</td>
                                            
                                            <td style="text-align:center">
                                                <span class="badge badge-success">Complete</span>
                                            </td>
                                            <td>
                                            <a class="btn-sm btn-success" title="Lihat Tindakan !" style="margin-right:5px" href="{{ route('lihat_tindakan') }}"> <i class="fa fa-eye" aria-hidden="true"></i></a>
                                                <a class="btn-sm btn-warning" title="Buat Tindakan!" style="margin-right:5px" href="{{ route('tindakan') }}"> <i class="fa fa-plus" aria-hidden="true"></i> Proses </a>
                                                
                                            </td>  
                                        </tr>
                                                    
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
			</section>
		</div>
        <!-- bagian body -->
        
        
</section>
@endsection
@section('css')
<link href="{{ asset('assets/node_modules/datatables/media/css/dataTables.bootstrap4.css') }}" rel="stylesheet">

@stop
@section('script')
<script src="{{ asset('assets/js/examples/examples.modals.js') }}"></script>
 <!-- This is data table -->
 <script src="{{ asset('assets/node_modules/datatables/datatables.min.js') }}"></script>
 <!-- start - This is for export functionality only -->
 <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
 <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
 <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
 <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
 <script src="{{ asset('assets/js/examples/examples.modals.js') }}"></script>
 <!-- end - This is for export functionality only -->
 <script>
 $(function() {
     $('#myTable').DataTable();
     $(function() {
         var table = $('#example').DataTable({
             "columnDefs": [{
                 "visible": false,
                 "targets": 2
             }],
             "order": [
                 [2, 'asc']
             ],
             "displayLength": 25,
             "drawCallback": function(settings) {
                 var api = this.api();
                 var rows = api.rows({
                     page: 'current'
                 }).nodes();
                 var last = null;
                 api.column(2, {
                     page: 'current'
                 }).data().each(function(group, i) {
                     if (last !== group) {
                         $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                         last = group;
                     }
                 });
             }
         });
         // Order by the grouping
         $('#example tbody').on('click', 'tr.group', function() {
             var currentOrder = table.order()[0];
             if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                 table.order([2, 'desc']).draw();
             } else {
                 table.order([2, 'asc']).draw();
             }
         });
     });
 });

 </script> 

@stop